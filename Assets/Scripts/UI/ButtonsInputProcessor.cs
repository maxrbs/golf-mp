using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonsInputProcessor : MonoBehaviour
{
    [SerializeField] private GameController _gameController;

    public void StartServer()
    {
        NetworkManager.Singleton.StartServer();
    }

    public void StartHost()
    {
        NetworkManager.Singleton.StartHost();
    }

    public void StartClient()
    {
        NetworkManager.Singleton.StartClient();
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void GoToMenu()
    {
        Disconnect();

        Scene currentScene = SceneManager.GetActiveScene(); 
        SceneManager.LoadScene(currentScene.buildIndex);

    }

    private void OnApplicationQuit() 
    {
        Disconnect();
        
    }

    private void Disconnect() 
    {
        NetworkManager.Singleton.Shutdown();
        
        Cleanup();
    }

    private void Cleanup()
    {
        if (NetworkManager.Singleton != null)
        {
            Destroy(NetworkManager.Singleton.gameObject);
        }
    }
}
