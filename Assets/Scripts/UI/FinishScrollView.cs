using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class FinishScrollView : NetworkBehaviour
{
    [SerializeField] private Transform _content;
    [SerializeField] private GameObject _slot;

    public void CreateSlot(PlayerStats playerInfo, int place)
    {
        GameObject newPlayerSlot = Instantiate(_slot, _content);
        newPlayerSlot.transform.GetChild(0).GetComponent<Text>().text = playerInfo.GetName();
        newPlayerSlot.transform.GetChild(1).GetComponent<Text>().text = place.ToString();
        //ThrowsCounter counter = FindObjectOfType<ThrowsCounter>();
        //newPlayerSlot.transform.GetChild(1).GetComponent<Text>().text = counter.GetCount().ToString();
    }

}
