using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;

public class GameController : NetworkBehaviour
{
    [SerializeField] private List<PlayerStats> _players;
    [SerializeField] private FinishScrollView _finishScroll;
    [SerializeField] private GameObject _endGameUI;
    [SerializeField] private Transform _spawnCenterPoint;
    [SerializeField] private float _spawnMaxOffset;
    
    private int _countFinishers;
    public UnityEvent OnFinishReach;
    private int _nextID;

    private void Awake()
    {
        _countFinishers = 0;
        _nextID = 0;
        _players = new List<PlayerStats>();
    }

    public void ProcessPlayerFinish(PlayerStats playerinfo)
    {
        print(playerinfo.GetName() + " is finished!!");

        _countFinishers++;

        playerinfo.gameObject.GetComponent<BallController>().DisableMovement();
        
        CreateNewPlayerSlot(playerinfo, _countFinishers);

        OnFinishReach.Invoke();

        //if (_countFinishers >= _players.Count)
        //if (_countFinishers >= _playersContainer.childCount)
        if (_countFinishers >= GetActivePlayers().Length)
            FinishGame();        
    }

    private void CreateNewPlayerSlot(PlayerStats playerinfo, int place)
    {
        _finishScroll.CreateSlot(playerinfo, place);
    }

    private void FinishGame()
    {
        print("Everyone reached finish");
        _endGameUI.SetActive(true);

        foreach (PlayerStats item in _players)
        {
            BallController playerController = item.gameObject.GetComponent<BallController>();
            playerController.DisableMovement();

        }
    }

    public void AddPlayer(PlayerStats stats)
    {
        _players.Add(stats);
        UpdatePlayersPositions();
    }

    public void AutoFillPlayerInfo(PlayerStats player)
    {
        int playerIndex = _players.IndexOf(player);
        
        _players[playerIndex].SetID(_nextID++);
        _players[playerIndex].SetName("����� " + (playerIndex + 1));
    }

    public List<PlayerStats> GetPlayersList()
    {
        return _players;
    }

    public PlayerStats GetPlayerByName(string name)
    {
        foreach (PlayerStats player in _players)
        {
            if (player.GetName().Equals(name))
                return player;
        }

        return null;
    }

    public Vector3 GetNewSpawnPosition() {
        Vector3 newPos = _spawnCenterPoint.position;
        newPos += new Vector3(Random.Range(-_spawnMaxOffset, _spawnMaxOffset), 0f, Random.Range(-_spawnMaxOffset, _spawnMaxOffset));
        
        return newPos;
    }

    private PlayerStats[] GetActivePlayers() {
        PlayerStats[] players = FindObjectsOfType<PlayerStats>();
        
        return players;
    }

    private void UpdatePlayersPositions()
    {
        foreach (PlayerStats item in _players)
        {
            item.gameObject.GetComponent<Rigidbody>().AddForce(Vector3.up * 0.5f, ForceMode.Impulse);
        }
    }
}
