using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using UnityEngine;
using System;

public class FinishHole : MonoBehaviour
{
    [SerializeField] private GameController _gameController;
    [SerializeField] private UnityEvent onFinish;

    private void OnTriggerEnter(Collider collision)
    {

        if (collision.gameObject.TryGetComponent(out PlayerStats playerInfo))
        {
            _gameController.ProcessPlayerFinish(playerInfo);

            onFinish.Invoke();

        }
    }
}
