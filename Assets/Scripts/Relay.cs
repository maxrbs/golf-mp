using System.Threading.Tasks;
using UnityEngine.UI;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using Unity.Services.Authentication;
using Unity.Services.Core;
using Unity.Services.Relay;
using Unity.Services.Relay.Models;
using UnityEngine;
using UnityEngine.Events;
using System;

public class Relay : MonoBehaviour
{
    [SerializeField] private Text _joinCodeText;
    [SerializeField] private InputField _codeInputField;
    [SerializeField] private const int _maxPlayersCount = 2;

    public UnityEvent OnJoinSuccess;
    public UnityEvent OnJoinFail;

    private UnityTransport _transport;

    private async void Awake()
    {
        _transport = FindObjectOfType<UnityTransport>();

        //await Authenticate();
    }

    private static async Task Authenticate()
    {
        await UnityServices.InitializeAsync();

        if (AuthenticationService.Instance.IsAuthorized)
            return;

        await AuthenticationService.Instance.SignInAnonymouslyAsync();
    }

    public async void CreateGame()
    {
        await Authenticate();

        Allocation a = await RelayService.Instance.CreateAllocationAsync(_maxPlayersCount);
        _joinCodeText.text = await RelayService.Instance.GetJoinCodeAsync(a.AllocationId);
        
        _transport.SetHostRelayData(a.RelayServer.IpV4, (ushort)a.RelayServer.Port, a.AllocationIdBytes, a.Key, a.ConnectionData);
        
        NetworkManager.Singleton.StartHost();
        
        OnJoinSuccess.Invoke();
    }

    public async void TryJoinGame()
    {
        await Authenticate();

        try
        {
            JoinAllocation a = await RelayService.Instance.JoinAllocationAsync(_codeInputField.text);

            _transport.SetClientRelayData(a.RelayServer.IpV4, (ushort)a.RelayServer.Port, a.AllocationIdBytes, a.Key, a.ConnectionData, a.HostConnectionData);
            _joinCodeText.text = _codeInputField.text.ToUpper();// = await RelayService.Instance.GetJoinCodeAsync(a.AllocationId);

            NetworkManager.Singleton.StartClient();
            
            OnJoinSuccess.Invoke();

        }
        catch (Exception)
        {
            OnJoinFail.Invoke();

        }        

    }
}
