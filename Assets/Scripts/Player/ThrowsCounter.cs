using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Unity.Netcode;

public class ThrowsCounter : MonoBehaviour
{
    [SerializeField] private Text _counterText;
    [SerializeField] private Text _counterInMenuText;

    public void SetNewCount(int count)
    {
        _counterText.text = count.ToString();
        _counterInMenuText.text = _counterText.text;
    }

    public int GetCount()
    {
        return int.Parse(_counterText.text);
    }

    public void ResetCount()
    {
        _counterText.text = "0";
        _counterInMenuText.text = "������: " + _counterText.text + ".";
    }

    public void AddCount(int addCount)
    {
        int count = int.Parse(_counterText.text);
        count += addCount;

        _counterText.text = count.ToString();
        _counterInMenuText.text = "������: " + _counterText.text + ".";
    }

}
