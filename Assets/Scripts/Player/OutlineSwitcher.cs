using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlineSwitcher : MonoBehaviour
{
    [SerializeField] private Outline _outlineComponent;
    //[SerializeField] private Material matUsual;
    //[SerializeField] private Material matHovered;

    private void Awake()
    {
        _outlineComponent.enabled = false;
        //gameObject.GetComponent<Renderer>().material = matUsual;        
    }

    private void OnMouseOver()
    {
        _outlineComponent.enabled = true;
        //gameObject.GetComponent<Renderer>().material = matHovered;
    }

    private void OnMouseExit()
    {
        _outlineComponent.enabled = false;
        //gameObject.GetComponent<Renderer>().material = matUsual;
    }
}
