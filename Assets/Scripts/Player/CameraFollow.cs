using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [Header("Object to follow")]
    [SerializeField] private Transform _followObject;
    [SerializeField] private Vector3 _objectOffset;

    [Header("Follow settings")]
    [SerializeField] private bool _isCanRotate;
    [SerializeField] private float _distance;
    [SerializeField] private float _rotateSpeed;
    [SerializeField] private float _moveSpeed;
    [SerializeField] private float _lookAtSpeed;
    [SerializeField] private Vector2 _upRotateHeightProcents;

    private void LateUpdate()
    {
        if (_followObject == null)
            return;

        MoveToObject();
        RotateCameraByMouse();
    }

    private void MoveToObject()
    {
        Vector3 newTargetPosition = _followObject.position;
        newTargetPosition += _objectOffset;

        if (Vector3.Distance(newTargetPosition, transform.position) - _distance > 0.5f)
        {
            transform.position = Vector3.Lerp(transform.position, newTargetPosition, _moveSpeed * Time.deltaTime);
        }

        if (Vector3.Distance(newTargetPosition, transform.position) - _distance < 0.5f)
        {
            newTargetPosition = newTargetPosition - transform.forward * _distance;
            transform.position = Vector3.Lerp(transform.position, newTargetPosition, _moveSpeed * Time.deltaTime);
        }
    }

    private void RotateCameraByMouse()
    {
        if (Input.GetMouseButton(0) && _isCanRotate)
        {
            Vector3 newTargetPosition = _followObject.position;
            newTargetPosition += _objectOffset;

            float sideRotation = _rotateSpeed * Input.GetAxis("Mouse X");
            float upRotation = _rotateSpeed * -Input.GetAxis("Mouse Y");

            transform.RotateAround(newTargetPosition, Vector3.up, sideRotation);

            if (transform.position.y - newTargetPosition.y > (_upRotateHeightProcents.y * _distance))
            {
                if (upRotation <= 0f)
                    transform.RotateAround(newTargetPosition, transform.right, upRotation);
            }
            else if (transform.position.y - newTargetPosition.y < (_upRotateHeightProcents.x * _distance))
            {
                if (upRotation >= 0f)
                    transform.RotateAround(newTargetPosition, transform.right, upRotation);
            }
            else
            {
                transform.RotateAround(newTargetPosition, transform.right, upRotation);
            }

            LookAtPoint(newTargetPosition);

            //transform.LookAt(newTargetPosition, Vector3.up);
        }
    }

    private void LookAtPoint(Vector3 newTargetPosition)
    {
        Vector3 lookDirection = newTargetPosition - transform.position;

        
        Quaternion endRotation = Quaternion.LookRotation(lookDirection);
        transform.rotation = Quaternion.Lerp(transform.rotation, endRotation, _lookAtSpeed * Time.deltaTime);

        //Vector3 targetRotation = Quaternion.LookRotation(lookDirection).eulerAngles;
        //Vector3 finalRotation = Vector3.Lerp(transform.rotation.eulerAngles, targetRotation, Time.deltaTime * _lookAtSpeed);
        //transform.rotation = Quaternion.Euler(finalRotation);
        
    }

    public void SetIsCanRotate(bool newState)
    {
        _isCanRotate = newState;
    }

    public void SetFollowObject(Transform transform)
    {
        _followObject = transform;
    }

}
