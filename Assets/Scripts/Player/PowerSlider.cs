using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PowerSlider : MonoBehaviour
{
    [SerializeField] private Slider _powerSlider;
    [SerializeField] private Image _sliderFillZone;
    [SerializeField] private float _speed;
    [SerializeField] private Color _minColor;
    [SerializeField] private Color _maxColor;

    [SerializeField] private bool _isWorking;

    private void Start()
    {
        StopWork();
    }

    private void Update()
    {
        if (_isWorking)
        {
            _sliderFillZone.color = Color.Lerp(_minColor, _maxColor, Mathf.PingPong(Time.time * _speed, 1));
            _powerSlider.value = Mathf.PingPong(Time.time * _speed, 1);

        }
    }

    public void StartWork()
    {
        if (_isWorking)
            return;

        _isWorking = true;
        SetChildrenActiveState(true);

    }

    public void StopWork()
    {
        _isWorking = false;
        SetChildrenActiveState(false);        
    }

    private void SetChildrenActiveState(bool newState)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(newState);
        }
    }

    public float GetCurrentValue()
    {
        return _powerSlider.value;
    }
}
