using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerReset : MonoBehaviour
{
    [SerializeField] private Vector3 _lastSafePosition;
    [SerializeField] private UnityEvent onResetPosition;

    public void SetSafePosition(Vector3 position)
    {
        _lastSafePosition = position;
    }

    public Vector3 GetSafePosition()
    {
        return _lastSafePosition;
    }

    public void ResetPlayer()
    {
        onResetPosition.Invoke();
        gameObject.transform.position = _lastSafePosition;
        gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
    }

}
