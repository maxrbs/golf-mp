using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine.UI;
using UnityEngine;

[Serializable]
public struct ScreenBorders
{
    public float Left;
    public float Right;
    public float Up;
    public float Down;
}

public class BallController : NetworkBehaviour
{
    [Header("Mouse input setup (in procents)")]
    [SerializeField] private ScreenBorders _screenBorders;
    private ScreenBorders _bordersInPixels;

    [Header("ThrowSettings")]
    [SerializeField] private float _maxStartAimVelocity;
    [SerializeField] private Vector2 _upDirectionAngleBorders;
    [SerializeField] private Vector2 _sideDirectionAngleBorders;
    [SerializeField] private float _throwPowerForTrajectory;
    [SerializeField] private bool _isConstTrajectory;
    [SerializeField] private float _throwMinPower;
    [SerializeField] private float _throwMaxPower;

    [Header("Refs")]
    [SerializeField] private CameraFollow _camFollowComponent;
    [SerializeField] private Rigidbody _rigidBodyComponent;
    [SerializeField] private PlayerReset _playerResetComponent;
    [SerializeField] private Transform _playerCamera;
    [SerializeField] private TrajectoryRenderer _trajectoryRenderer;
    [SerializeField] private PowerSlider _powerSlider;
    [SerializeField] private ThrowsCounter _throwsCounter;

    [Header("For editor visualize only")]
    [SerializeField] private Vector3 _throwDirection;
    [SerializeField] private bool _isMouseOnObject;
    [SerializeField] private Vector3 _startRotation;
    [SerializeField] private bool _isAimingStarted;
    [SerializeField] private bool _isMovementEnabled;


    private void Start()
    {
        if (!IsOwner)
            return;

        GetReferences();
        SetValues();

    }

    private void GetReferences()
    {
        _bordersInPixels = GetScreenBordersInPixels(_screenBorders);
        _powerSlider = FindObjectOfType<PowerSlider>();
        _throwsCounter = FindObjectOfType<ThrowsCounter>();

        SetCameraForPlayer();
    }

    private void SetValues()
    {
        _throwDirection = new Vector3(0, 0, 0);
        _isMouseOnObject = false;
        _isAimingStarted = false;
        _isMovementEnabled = true;
        
        _throwsCounter.ResetCount();
    }

    #region MouseEvents
    private void OnMouseDown()
    {
        if (!IsOwner)
            return;

        if (!_isMovementEnabled)
            return;

        _camFollowComponent.SetIsCanRotate(false);

        if (_rigidBodyComponent.velocity.magnitude > _maxStartAimVelocity)
            return;

        _isAimingStarted = true;

        UpdateRotationFromCamPosition();
    }

    private void OnMouseDrag()
    {
        if (!IsOwner)
            return;

        if (!_isMovementEnabled)
            return;

        if (_isMouseOnObject || _rigidBodyComponent.velocity.magnitude > _maxStartAimVelocity)
            return;

        if (!_isAimingStarted)
        {
            _isAimingStarted = true;
            UpdateRotationFromCamPosition();
        }
        
        CalculateDirection();
        transform.rotation = Quaternion.Euler(_throwDirection + _startRotation);

        float power = _powerSlider.GetCurrentValue() * (_throwMaxPower - _throwMinPower) + _throwMinPower;
        _powerSlider.StartWork();

        if (_isConstTrajectory)
            _throwPowerForTrajectory = (_throwMinPower + _throwMaxPower) / 2f;
        else
            _throwPowerForTrajectory = power;

        _trajectoryRenderer.ShowTrajectory(transform.position, transform.forward * _throwPowerForTrajectory * (1f / _rigidBodyComponent.mass));

    }

    private void OnMouseUp()
    {
        if (!IsOwner)
            return;

        if (!_isMovementEnabled)
            return;

        _camFollowComponent.SetIsCanRotate(true);
        _trajectoryRenderer.HideTrajectory();
        _powerSlider.StopWork();

        _isAimingStarted = false;

        if (_rigidBodyComponent.velocity.magnitude > _maxStartAimVelocity || _isMouseOnObject)
            return;

        float power = _powerSlider.GetCurrentValue() * (_throwMaxPower - _throwMinPower) + _throwMinPower;

        _playerResetComponent.SetSafePosition(transform.position);

        _rigidBodyComponent.AddForce(transform.forward * power, ForceMode.Impulse);

        _throwsCounter.AddCount(1);
    }

    private void OnMouseExit()
    {
        if (!IsOwner)
            return;

        _isMouseOnObject = false;
    }

    private void OnMouseEnter()
    {
        if (!IsOwner)
            return;

        _trajectoryRenderer.HideTrajectory();
        _powerSlider.StopWork();

        _isMouseOnObject = true;
    }
    #endregion

    private void SetCameraForPlayer()
    {
        Camera cam = FindObjectOfType<Camera>();
        _playerCamera = cam.transform;
        _camFollowComponent = cam.GetComponent<CameraFollow>();
        _camFollowComponent.SetFollowObject(transform);
        //cam.transform.parent = gameObject.transform.parent;

    }

    private void UpdateRotationFromCamPosition()
    {
        Vector3 lookAtPosition = _playerCamera.position;
        lookAtPosition.y = transform.position.y;

        transform.LookAt(lookAtPosition);
        transform.Rotate(Vector3.up * 180);

        _startRotation = transform.rotation.eulerAngles;
    }

    public void OnResetPlayer()
    {
        UpdateRotationFromCamPosition();
    }

    private void CalculateDirection()
    {
        Vector2 pointerPosition = GetClampedVectorToScreenBorders(Input.mousePosition, _bordersInPixels);

        Vector3 rawMouseDirectionInProcent = Vector3.zero; //transform.forward

        rawMouseDirectionInProcent.x = (pointerPosition.x - _bordersInPixels.Left) / (_bordersInPixels.Left - _bordersInPixels.Right);
        rawMouseDirectionInProcent.y = (pointerPosition.y - _bordersInPixels.Down) / (_bordersInPixels.Up - _bordersInPixels.Down);
        rawMouseDirectionInProcent.z = rawMouseDirectionInProcent.y;

        _throwDirection = GetDirectionInAngles(rawMouseDirectionInProcent, _upDirectionAngleBorders, _sideDirectionAngleBorders);

    }

    private Vector3 GetDirectionInAngles(Vector3 rawMouseDirectionInProcent, Vector2 upDirectionAngleBorders, Vector2 sideDirectionAngleBorders)
    {
        Vector3 directRotation;

        // X Direction - rotation by Y-axis
        // Y Direction - rotation by X-axis
        // Z Direction - forward

        directRotation.x = -(rawMouseDirectionInProcent.y * (upDirectionAngleBorders.y - upDirectionAngleBorders.x) + upDirectionAngleBorders.x);
        directRotation.y = -40 + 180 + rawMouseDirectionInProcent.x * (sideDirectionAngleBorders.y - sideDirectionAngleBorders.x) + sideDirectionAngleBorders.x;
        directRotation.z = 0;

        return directRotation;
    }

    private Vector2 GetClampedVectorToScreenBorders(Vector2 sourceVec, ScreenBorders borders)
    {
        if (sourceVec.x < borders.Left) sourceVec.x = borders.Left;
        if (sourceVec.x > borders.Right) sourceVec.x = borders.Right;

        if (sourceVec.y > borders.Up) sourceVec.y = borders.Up;
        if (sourceVec.y < borders.Down) sourceVec.y = borders.Down;

        return sourceVec;
    }

    private ScreenBorders GetScreenBordersInPixels(ScreenBorders bordersInProcents)
    {
        ScreenBorders tempBorders;

        tempBorders.Left = Screen.width * bordersInProcents.Left / 100f;
        tempBorders.Right = Screen.width - (Screen.width * bordersInProcents.Right / 100f);

        tempBorders.Up = Screen.height - (Screen.height * bordersInProcents.Up / 100f);
        tempBorders.Down = Screen.height * bordersInProcents.Down / 100f;

        return tempBorders;
    }

    public void DisableMovement()
    {
        _isMovementEnabled = false;

    }

    public void EnableMovement()
    {
        _isMovementEnabled = true;

    }
}
