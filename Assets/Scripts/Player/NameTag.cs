using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NameTag : MonoBehaviour
{
    [SerializeField] private Text _nameText; 
    [SerializeField] private Transform _camera;
    [SerializeField] private Transform _followPosition;
    [SerializeField] private Vector3 _offset;

    private void Start()
    {
        _camera = Camera.main.transform;
    }

    private void LateUpdate()
    {
        transform.position = _followPosition.position + _offset;
        transform.LookAt(transform.position + _camera.rotation * Vector3.forward, _camera.rotation * Vector3.up);
    }

    public void SetName(string name)
    {
        _nameText.text = name;
    }
}
