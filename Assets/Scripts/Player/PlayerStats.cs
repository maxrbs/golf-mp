using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class PlayerStats : NetworkBehaviour
{
    [SerializeField] private string _name;
    [SerializeField] private int _id;
    [SerializeField] private NameTag _nameTag;

    private void Start()
    {
        GameController gc = FindObjectOfType<GameController>();
        gc.AddPlayer(this);
        gc.AutoFillPlayerInfo(this);
        
        transform.position = gc.GetNewSpawnPosition();
        print("Spawned: " + _name + " (id=" + _id + ")");
    }

    public string GetName()
    {
        return _name;
    }

    public void SetName(string newName)
    {
        _name = newName;
        _nameTag.SetName(newName);
    }


    public int GetID()
    {
        return _id;
    }

    public void SetID(int newID)
    {
        _id = newID;
    }

}
