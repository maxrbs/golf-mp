using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrajectoryRenderer : MonoBehaviour
{
    [SerializeField] private int _trajectoryPointCount;
    private LineRenderer _lineRendererComponent;

    private void Start()
    {
        _lineRendererComponent = GetComponent<LineRenderer>();
    }

    public void ShowTrajectory(Vector3 origin, Vector3 velocity)
    {
        _lineRendererComponent.enabled = true;

        Vector3[] points = new Vector3[_trajectoryPointCount];

        _lineRendererComponent.positionCount = points.Length;

        for (int i = 0; i < points.Length; i++)
        {
            float time = i * 0.1f;
            points[i] = origin + velocity * time + Physics.gravity * time * time / 2f;

            if (points[i].y < origin.y - 5f)
            {
                _lineRendererComponent.positionCount = i;

                break;
            }
        }

        _lineRendererComponent.SetPositions(points);
    }

    public void HideTrajectory()
    {
        _lineRendererComponent.enabled = false;
    }
}
