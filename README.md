# Golf Multiplayer
## Description
### RU
Прототип игры Гольф по сети.

Для игры по сети необходимо:
1) Первому игроку создать игру и передать код комнаты (в правом нижнем углу экрана) другу
2) Второму игроку подключиться к сессии по полученному коду.

Для управления мячом - «оттянуть» его и подобрать силу удара. 
Кто первый доберется до финиша – тот и победил


### EN
Prototype of the Golf game over the network.

To play online you need:
1) The first player to create a game and transfer the room code (in the lower right corner of the screen) to a friend
2) The second player connects to the session using the received code.

To control the ball - "pull" it and select the force of throwing.
Who gets to the finish line first wins


## Screenshots
![Screen1](/Sharing/Screenshots/1.PNG)
![Screen2](/Sharing/Screenshots/2.PNG)
![Screen3](/Sharing/Screenshots/3.PNG)
![Screen4](/Sharing/Screenshots/4.PNG)

## Links
Build on itch: https://maxrbs.itch.io/golf-multiplayer